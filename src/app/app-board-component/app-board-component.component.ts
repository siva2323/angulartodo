import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-app-board-component',
  templateUrl: './app-board-component.component.html',
  styleUrls: ['./app-board-component.component.css']
})
export class AppBoardComponentComponent implements OnInit {
squares=Array(9).fill(null);
XisNext:boolean=false;
winner:string="";

constructor(){}

ngOnInit(){
    this.newGame();
}
newGame(){
    this.squares=Array(9).fill(null);
    this.XisNext=true;
    this.winner="";
}
getPlayer(){
    return (this.XisNext?'X':'')
}

}