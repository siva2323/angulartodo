import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBoardComponentComponent } from './app-board-component.component';

describe('AppBoardComponentComponent', () => {
  let component: AppBoardComponentComponent;
  let fixture: ComponentFixture<AppBoardComponentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppBoardComponentComponent]
    });
    fixture = TestBed.createComponent(AppBoardComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
