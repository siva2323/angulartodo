import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'ToDo list';
    list: any[] = [];
    newTask: string = "";

    addTask(newTask: string) {
        this.newTask="";
        this.list.push({ id: this.list.length, value: newTask });
    }
    removeTask(selectedId:number){
        this.list=(this.list).filter(item=>item.id !=selectedId)
    }

}
