import { Component,Input } from '@angular/core';

@Component({
  selector: 'app-app-square-component',
  templateUrl: './app-square-component.component.html',
  styleUrls: ['./app-square-component.component.css']
})
export class AppSquareComponentComponent {
@Input() 
value: 'X' | 'O'="X"
}
