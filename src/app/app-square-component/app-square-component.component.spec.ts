import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSquareComponentComponent } from './app-square-component.component';

describe('AppSquareComponentComponent', () => {
  let component: AppSquareComponentComponent;
  let fixture: ComponentFixture<AppSquareComponentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppSquareComponentComponent]
    });
    fixture = TestBed.createComponent(AppSquareComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
